import cv2
import numpy as np

def colorFlames(region):
    region = cv2.medianBlur(region, 3)
    b, g, r = cv2.split(region)
    hsv = cv2.cvtColor(region, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    dstFire = (r >= g) & (g >= b) & (s > 100) & (r > 180) & (v > 200)

    mask = np.asarray(dstFire).astype(int)

    red_count = cv2.countNonZero(mask)
    red = red_count / (region.size/3)

    return red