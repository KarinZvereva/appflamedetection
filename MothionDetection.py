import cv2
import numpy as np
import ColorDetection as color
from math import sqrt

class RegionFlame:
    def __init__(self, area, redCount, x, y, w, h):
        self.area = area
        self.redCount = redCount
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def insideRegion(self, regions):

        inside = False
        for i in range(len(regions)):
            elem = regions[i]
            if (
                    self.x > elem.x and self.x + self.w < elem.x + elem.w and self.y > elem.y and self.y + self.h < elem.y + elem.h):
                inside = True
                break
        return inside

    def getPreviousRegion(self, prevRegions):
        regionsDist = []
        for r in prevRegions:
            dist = sqrt(((self.x + self.w / 2) - (r.x + r.w / 2)) ** 2 + ((self.y + self.h / 2) - (r.y + r.h / 2)) ** 2)
            c = sqrt((self.w / 2 + 50) ** 2 + (self.h / 2 + 50) ** 2)
            if dist < c:
                regionsDist.append(previousContour(dist, r.h))

        regionsDist.sort(key=lambda region: region.dist, reverse=True)

        if len(regionsDist) > 0:
            return regionsDist[0]
        return None


class previousContour:
    def __init__(self, dist, h):
        self.dist = dist
        self.h = h

# cap = cv2.VideoCapture('noFire.mp4')
previos_regions = None
first_frame = None
step = 1
background_frames = None
N = 600

# cap = cv2.VideoCapture('test1.mp4')
# cap = cv2.VideoCapture('test4.mp4')
cap = cv2.VideoCapture('test5.mp4')



while (cap.isOpened()):
    ret, frame = cap.read()
    if ret == False:
        break;
    frame = cv2.resize(frame, (N, int(N * frame.shape[0] / frame.shape[1])))
    frameView = frame.copy()
    frameView1 = frame.copy()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = cv2.medianBlur(gray, 3)
    areaFrame = frameView.size / 3

    if first_frame is None:
        first_frame = gray
        continue

    delta_frame = cv2.absdiff(first_frame, gray)
    ret, thresh_frame = cv2.threshold(delta_frame, 65, 255, cv2.THRESH_BINARY)

    thresh_frame = cv2.erode(thresh_frame, None, 0)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (1, 4), (0, 2))
    thresh_frame = cv2.dilate(thresh_frame, kernel, 1)
    # cv2.imshow("Subtraction background", thresh_frame)
    contours, hierarchy = cv2.findContours(thresh_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    regions = []
    # cv2.drawContours(frameView, contours, -1, (0, 255, 0), 1)
    for contour in contours:
        (x, y, w, h) = cv2.boundingRect(contour)

        if w * h < areaFrame * 0.8:
            cropped = frame[y:y + h, x:x + w]
            red = color.colorFlames(cropped)
            if red >= 0.1:
                regions.append(RegionFlame(cv2.contourArea(contour), red, x, y, w, h))

    regions.sort(key=lambda r: r.area, reverse=True)
    maxAreaRegions = regions[0:3]

    for i in maxAreaRegions:
        if not i.insideRegion(maxAreaRegions):
            prevRegion = i.getPreviousRegion(previos_regions)
            if prevRegion is not None:
                if abs(prevRegion.h - i.h) / prevRegion.h > 0.2:
                    cv2.rectangle(frameView, (i.x, i.y), (i.x + i.w, i.y + i.h), (255, 0, 0), 1)
                    print("Fire")

    cv2.imshow("Video", frameView)
    previos_regions = maxAreaRegions


    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
